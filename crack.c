#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
 int tryguess(char *hash, char *guess)
    {
       // Hash the guess using MD5
       char* hashes;
        hashes = md5(guess, strlen(guess));
      if (strcmp( hashes, hash ) == 0)
        {
         return 1;
        }
        else
        {
      return 0;
      }
    // Compare the two hashes

    // Free any malloc'd memory
      
       free(hashes);
    }



 

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
     //Get size of file
    struct stat info;
    stat(filename, &info);
    int file_length = info.st_size;
    
    printf("Size: %d\n", file_length);
    
    // Allocate space for the entire file
    char *chunk = (char *)malloc(file_length+1);
    
    // Read in entire file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        perror("Can't open file");
        exit(1);
    }
    fread(chunk, 1, file_length, f);
    fclose(f);
    
    // Add null character to end
    chunk[file_length] = '\0';
    
    // Change newlines to nulls, counting as we go
    int lines = 0;
    for (int i = 0; i < file_length; i++)
    {
        if (chunk[i] == '\n') 
        {
            chunk[i] = '\0';
            lines++;
        }
    }
    
    // If the file didn't have a final terminating newline,
    // it won't be counted. Let's check for that situation
    // and adjust the linecount accordingly. Since we've already
    // changed newlines to null, all we need to do is look
    // for a null character at the end of the chunk.
    if (chunk[file_length-1] != '\0') lines++;
    
    // Allocate space for an array of pointers
    char **strings = malloc((lines+1) * sizeof(char *));
    
    // Loop through chunk, storing pointers to each string
    int string_idx = 0;
    for (int i = 0; i < file_length; i += strlen(chunk+i) + 1)
    {
        strings[string_idx] = chunk+i;
        string_idx++;
    }
    
    // Store terminating NULL
    strings[lines] = NULL;
    
    return strings;
    
    free(chunk); 
    free(strings); 
   
    }//end readfile
    


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int h = 0;
    while (hashes[h] != NULL )
    {   
      for(int i = 0; dict[i] != NULL; i++)
        if(tryguess(hashes[h], dict[i])==1)
          {
            printf("%s\n",dict[i]);
          }
       h++;
         
    }
   
free(*hashes);
free(hashes);
free(*dict);
free(dict);


}
    
  

